var $ = $;

$(document).ready(function () {
  var clientHeight = $('.header')[0].clientHeight;
  var stopClick = false;
  var masterIndex = null;

  var calcSlider = function (i) {
    $('.slider').css({
      width: $('.nav-link')[i].clientWidth + 'px',
      marginLeft:
        $('.nav-link')[i].offsetLeft - $('.nav-wrapper')[0].offsetLeft + 'px',
      top:
        $('.nav-link')[i].offsetTop + $('.nav-link')[i].clientHeight - 5 + 'px',
    });
    $('.nav-link').removeClass('selected');
    $($('.nav-link')[i]).addClass('selected');
    masterIndex = i;
  };

  $(window)
    .scroll(function () {
      var self = this;
      $('.slide').each(function (i, v) {
        if (!stopClick) {
          var divPos = v.offsetTop - clientHeight;
          if (
            self.pageYOffset >= divPos &&
            self.pageYOffset < divPos + v.clientHeight
          ) {
            calcSlider(i);
          }
        }
      });
    })
    .resize(function () {
      calcSlider(masterIndex);
    });
  $(window).scroll();

  //nav click listener
  $('.nav-link').click(function () {
    $('.nav-wrapper').addClass('stop-click');
    stopClick = true;
    var id = $(this).index();

    calcSlider(id);

    $('body').animate(
      {
        scrollTop: $('.slide')[id].offsetTop - clientHeight + 1,
      },
      {
        duration: 500,
        easing: 'swing',
        complete: function () {
          $('.nav-wrapper').removeClass('stop-click');
          stopClick = false;
        },
      }
    );
  });

  $('.clientForm').submit(function () {
    if (grecaptcha.getResponse()) {
      $.ajax({
        type: 'POST',
        url: 'email.php',
        data: $('.clientForm').serialize(),
        success: function (response) {
          if (response) {
            $('#returnMessage').html(
              "<span style='color:green;font-size:2em;'>Message Sent!</span>"
            );
          } else {
            $('#returnMessage').html(
              "<span style='color:red;font-size:2em;'>Message send failed!</span>"
            );
          }
          var timer = setTimeout(function () {
            $('.clientForm').slideUp();
            clearTimeout(timer);
          }, 2000);
        },
      });
    }
    return false;
  });
});

var onloadCallback = function () {
  grecaptcha.render('google_recaptcha', {
    sitekey: '6LcHaF4dAAAAAEbfBj4IG5Iokme9CMgt-g_TH1nA',
  });
};
